from binance.client import Client
client = Client("w4i30T7xWjqRlJIAsATAmkRpDtewPcP6cghhlvx8r5WFt02tOEbKdn97P3PkYKr3",
    "i6KhvCSUVQcuoQaXp52qxWA0vRkvRKDLStKL87BhS3T41lnmy5f3pgOXTb1OQCAM")
from binance.enums import *
import random
SYMBOL_TYPE_SPOT = 'SPOT'
ORDER_STATUS_NEW = 'NEW'
ORDER_STATUS_PARTIALLY_FILLED = 'PARTIALLY_FILLED'
ORDER_STATUS_FILLED = 'FILLED'
ORDER_STATUS_CANCELED = 'CANCELED'
ORDER_STATUS_PENDING_CANCEL = 'PENDING_CANCEL'
ORDER_STATUS_REJECTED = 'REJECTED'
ORDER_STATUS_EXPIRED = 'EXPIRED'
KLINE_INTERVAL_1MINUTE = '1m'
KLINE_INTERVAL_2MINUTE = '3m'
KLINE_INTERVAL_5MINUTE = '5m'
KLINE_INTERVAL_15MINUTE = '15m'
KLINE_INTERVAL_30MINUTE = '30m'
KLINE_INTERVAL_1HOUR = '1h'
KLINE_INTERVAL_2HOUR = '2h'
KLINE_INTERVAL_4HOUR = '4h'
KLINE_INTERVAL_6HOUR = '6h'
KLINE_INTERVAL_8HOUR = '8h'
KLINE_INTERVAL_12HOUR = '12h'
KLINE_INTERVAL_1DAY = '1d'
KLINE_INTERVAL_3DAY = '3d'
KLINE_INTERVAL_1WEEK = '1w'
KLINE_INTERVAL_1MONTH = '1M'
SIDE_BUY = 'BUY'
SIDE_SELL = 'SELL'
ORDER_TYPE_LIMIT = 'LIMIT'
ORDER_TYPE_MARKET = 'MARKET'
TIME_IN_FORCE_GTC = 'GTC'
TIME_IN_FORCE_IOC = 'IOC'

# get all orders
def get_all_orders():
    orders = client.get_all_orders(symbol='LINKETH', limit=100)
    orders = sorted(orders, key=lambda k: k['price'], reverse=True)
    print("get_all_orders {}".format(orders))
    for order in orders:
        print(order['price'] + "    " + order['side'] + order['origQty'] + "  LINK")


# wash trading
def wash_trading():
    for a in range(2):
        wash_price = 0.0013282
        wash_quantity = random.randint(100, 500)
        order = client.create_order(
            symbol='LINKETH',
            side=SIDE_SELL,
            type=ORDER_TYPE_LIMIT,
            timeInForce=TIME_IN_FORCE_GTC,
            quantity=wash_quantity,
            price=wash_price)
        order = client.create_order(
            symbol='LINKETH',
            side=SIDE_BUY,
            type=ORDER_TYPE_LIMIT,
            timeInForce=TIME_IN_FORCE_GTC,
            quantity=wash_quantity,
            price=wash_price)

# singel buy or sell
# TODO need to read trade history, and control the total buy amount
# Query order (SIGNED) from Official API, link at below
def single_buy_or_sell():
    for a in range(2):
        wash_price = 0.0013282
        wash_quantity = random.randint(10, 20)

        order = client.create_order(
            symbol='LINKETH',
            side=SIDE_BUY,
            type=ORDER_TYPE_LIMIT,
            timeInForce=TIME_IN_FORCE_GTC,
            quantity=wash_quantity,
            price=wash_price)

# their API document !!
# https://www.binance.com/restapipub.html
# https:
# //github.com / sammchardy / python - binance
