import time
from binance.client import Client
client = Client("w4i30T7xWjqRlJIAsATAmkRpDtewPcP6cghhlvx8r5WFt02tOEbKdn97P3PkYKr3","i6KhvCSUVQcuoQaXp52qxWA0vRkvRKDLStKL87BhS3T41lnmy5f3pgOXTb1OQCAM")
from depositable import binance_check_token_depositable
from new_listing import binance_listen_new_listing,etherdelta_tradable_check

BINANCE_TRADEABLE = "binance_tradable"
BINANCE_DEPOSITABLE = "binance_depositable"
ETHERDELTA_TRADEABLE = "etherdelta_tradeable"


# Read tasks from a file
def read_tasks():
    token_f = open("task_token.txt")
    tasks = dict()
    for t in token_f:
        token = t.strip()
        tasks[token] = {BINANCE_TRADEABLE:False,BINANCE_DEPOSITABLE:False,ETHERDELTA_TRADEABLE:False}
    token_f.close()
    return tasks

from mail import send_simple_message
def send_email(token,message,exchange="Binance Bot"):
    res = send_simple_message(token,message,exchange)
    print("\t\t{}".format(res))

tasks = read_tasks()
print(tasks)
def check_token_status():
    all_true = True
    etherdelta_not_tradable_tokens = []
    for token in tasks:
        t_binance_depositable = tasks[token][BINANCE_DEPOSITABLE]
        t_binance_tradable = tasks[token][BINANCE_TRADEABLE]
        t_etherdelta_tradable = tasks[token][ETHERDELTA_TRADEABLE]

        if not t_binance_depositable:
            print("{} not t_binance_depositable".format(token))
            all_true = False
            if binance_check_token_depositable(client,token):
                send_email(token,"{} is depositalbe on binance now!".format(token))
                print("\t\t{} is depositalbe on binance now!".format(token))
                tasks[token][BINANCE_DEPOSITABLE] = True

        if not t_binance_tradable:
            print("{} not t_binance_tradable".format(token))
            all_true = False
            if binance_listen_new_listing(client,token):
                send_email(token,"{} is tradable on binance now!".format(token))
                print("\t\t{} is tradable on binance now!".format(token))
                tasks[token][BINANCE_TRADEABLE] = True

        if not t_etherdelta_tradable:
            print("{} not t_etherdelta_tradable".format(token))
            all_true = False
            etherdelta_not_tradable_tokens.append(token)

    results = etherdelta_tradable_check(etherdelta_not_tradable_tokens)
    if len(results) != 0:
        for result in results:
            token,key,json = result
            send_email(token,"{} is tradable on etherdelta now!. meta:{}".format(key,json),"Etherdelta bot")
            print("\t\t{} is tradable on etherdelta now!. meta:{}".format(key,json))
            tasks[token][ETHERDELTA_TRADEABLE] = True

    return all_true

small_counter = 0

while True:
   print(small_counter)
   small_counter += 1
   s =  check_token_status()
   if s: break
   time.sleep(5)
