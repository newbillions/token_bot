# listen new listing
def binance_listen_new_listing(client,token_symbol):
    tickers = client.get_all_tickers()
    for ticker in tickers:
        symbol = ticker['symbol']
        if (symbol == '{}ETH'.format(token_symbol) or symbol == '{}BTC'.format(token_symbol)):
            return True
    return False

import requests
def etherdelta_tradable_check(symbols):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    r = requests.get('https://cache.etherdelta.com/returnTicker', headers=headers)
    results = []
    while r.status_code != 200:
        print(r.status_code)
        r = requests.get('https://cache.etherdelta.com/returnTicker')
    try:
        json = r.json()
    except:
        return results
    print(json)
    for key in json:
        lower_key = key.lower()
        for symbol in symbols:
            lower_symbol = symbol.lower()
            if "eth_{}".format(lower_symbol) in lower_key:
                results.append([symbol,key,json[key]])
    return results

# import websocket
#
# def on_message(ws, message):
#     print("message:{}".format(message))
#
# def on_error(ws, error):
#     print("error:{}".format(error))
#
# def on_close(ws):
#     print("### closed ###")
#
#
# if __name__ == "__main__":
#     # websocket.enableTrace(True)
#     # ws = websocket.WebSocketApp(
#     #     "wss://socket.etherdelta.com/socket.io/?transport=websocket",
#     #                           on_message = on_message,
#     #                           on_error = on_error,
#     #                           on_close = on_close)
#     # ws.run_forever()
#     from websocket import create_connection
#     ws = create_connection("wss://socket.etherdelta.com/socket.io/?transport=websocket")
#     print("Sending 'Hello, World'...")
#     ws.send("getMarket")
#     print("Sent")
#     print("Receiving...")
#     result =  ws.recv()
#     print("Received '%s'" % result)
#     ws.close()
