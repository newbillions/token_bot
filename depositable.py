# Check to see if a token is depositable
def binance_check_token_depositable(client,token):
    try:
        info = client.get_account()
    except:
        return False
    for l in info['balances']:
        if l['asset'] == token:
            return True
    return False
